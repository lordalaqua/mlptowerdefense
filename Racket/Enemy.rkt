#lang racket

(require "Point.rkt")
(require "Entity.rkt")
(require "Tower.rkt")
(require "Draw.rkt")
(provide (all-defined-out))

; The enemy is an entity with some extra stats:
; From entity:
; pos : vector - enemy position
; text : string - text "sprite" to draw
; angle : number (radians) - angle to draw
; From Enemy
; hp : number - enemy vitality, hit points
; speed-dir : vector - unit vector, direction of speed
; speed-val : number - scalar, magnitude of speed
; path : list vector - path enemy will follow to base
; id : number - enemy identifier
(struct enemy entity (hp speed-dir speed-val path id))

; Move enemy based on its current speed and a time interval
(define (move-enemy this time)
    (enemy
        (pt+ (entity-pos this) (pt*s (enemy-speed-dir this) (* time (enemy-speed-val this))))
        (entity-text this)
        (entity-angle this)
        (enemy-hp this)
        (enemy-speed-dir this)
        (enemy-speed-val this)
        (enemy-path this)
        (enemy-id this)))

; Damage the enemy by an amount given as argument
(define (damage-enemy this damage)
    (enemy
        (entity-pos this)
        (entity-text this)
        (entity-angle this)
        (- (enemy-hp this) damage)
        (enemy-speed-dir this)
        (enemy-speed-val this)
        (enemy-path this)
        (enemy-id this)))

; Update the enemy at every clock tick
; Towers are needed to check which enemies are being targeted and damages by the towers
(define (update-enemy this time towers)
    ; Update enemy speed and path
    (define-values (new-pos new-path new-speed-dir)
        (update-enemy-path-and-speed this))
    ; Move updated enemy and return result
    (move-enemy
        (enemy
            new-pos
            (entity-text this)
            (entity-angle this)
            (- (enemy-hp this) (enemy-calculate-damage this towers))
            new-speed-dir
            (enemy-speed-val this)
            new-path
            (enemy-id this))
        time))

; Calculate the damage done to an enemy by the towers
(define (enemy-calculate-damage this towers)
    ; Fold towers accumulating damage
    (foldl
        (lambda (tower result) (+ result
            ;Then fold each tower's targets
            (foldl
                (lambda (target result) (+ result (cond
                    ; If enemy is being targeted by tower, accumulate damage
                    [(= (enemy-id target) (enemy-id this))
                        (tower-attack tower)]
                    ; Otherwise no damage
                    [else 0])))
                0 (tower-targets tower))))
        0 towers))

; Update enemy speed and position based on the path it is following
(define (update-enemy-path-and-speed this)
    (match (enemy-path this)
        ; If Path is empty, no speed, reached base
        [(? empty?) (values (entity-pos this) empty (pt 0 0))]
        ; Path has only last point, make it empty, reached base
        [(cons head (? empty?)) (values (entity-pos this) empty (pt 0 0))]
        ; List has more than one element, enemy is still moving
        [(cons head tail)
            (cond
                ; If enemy is still on first previous line, do nothing
                [(pt-in-line? (entity-pos this) head (first tail))
                    (values (entity-pos this) (enemy-path this) (enemy-speed-dir this))]
                ; If enemy has reached end of line (or passed it)
                ; Remove first point from path and recalculate speed
                [else
                    (values
                        (first tail) ; New enemy position is first node of new path
                        tail ; Path is tail of last path
                        ; Update speed direction:
                        (match tail
                            ; If new path has only one node, no speed, base
                            ; was reached
                            [(cons t-head (? empty?)) (pt 0 0)]
                            ; Otherwise, normalize distance between first two
                            ; path points for speed direction
                            [(cons t-head t-tail)
                                (pt-normalized
                                    (pt- (second tail) (first tail)))]))])]))

; Enemy spawner
; Spawn list of enemies, sending parameters to spawn enemy
(define (spawn-wave path wave-size time-interval kill-count)
    (build-list wave-size (lambda (v) (spawn-enemy time-interval kill-count path))))

; Keep track of how many enemies were spawned for the enemy-id
(define spawned 0)

; Spawn one enemy, difficulty based on kill-count and time-interval
(define (spawn-enemy time-interval kill-count path)
    (set! spawned (+ spawned 1))
    (define-values (name hp speed) (get-random-enemy time-interval kill-count))
    (enemy
        (first path)    ; Starting position
        name            ; Name
        0               ; Angle on which to draw
        hp              ; Hit points
        (pt-normalized (pt- (second path) (first path)))    ; Starting direction
        (+ (* speed 0.05) (* kill-count 0.001))             ; Speed magnitude
        path            ; Path which enemy will follow
        spawned))       ; Enemy id

; Spawn different enemies randomly based on kill count and interval between waves
(define (get-random-enemy time kill-count)
    (define rng (remainder (exact-round (+ (random 100) (+ kill-count (/ 100 time)))) 100))
    (cond
        [(< rng 30)
            (values "C" 20 3)]
        [(< rng 50)
            (values "Java" 50 2.2)]
        [(< rng 70)
            (values "C#" 50 2)]
        [(< rng 80)
            (values "C++" 15 4)]
        [(< rng 90)
            (values "Ruby" 15 1)]
        [else
            (values "{C}" 100 2)]))
