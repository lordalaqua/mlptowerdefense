#lang racket

(require racket/gui)
(provide (all-defined-out))

; Drawing "assets" used by the game, colors, pens, brushes and fonts

; Colors
(define black (make-color 0 0 0))
(define green (make-color 0 255 0))
(define dark-green (make-color 0 50 0))
(define dark-red (make-color 50 0 0))

; Brushes
(define no-brush (make-object brush% black 'transparent))
(define black-brush (make-object brush% black 'solid))
(define green-brush (make-object brush% green 'solid))
(define dark-green-brush (make-object brush% dark-green 'solid))

; Pens
(define no-pen (make-object pen% black 1 'transparent))
(define dark-red-pen (make-object pen% dark-red 2 'long-dash))
(define red-pen (make-object pen% "RED" 2 'solid))
(define dot-pen (make-pen #:color green #:width 2 #:style 'long-dash))
(define dark-pen (make-pen #:color dark-green #:width 2 #:style 'long-dash))

; Fonts
(define main-font (make-font #:size 14 #:face "Courier" #:weight 'normal #:family 'modern))
(define dark-font (make-font #:size 20 #:face "Courier" #:weight 'normal #:family 'modern))
(define red-font (make-font #:size 20 #:face "Courier" #:weight 'normal #:family 'modern))
(define large-font (make-font #:size 20 #:face "Courier" #:weight 'normal #:family 'modern))
(define base-font (make-font #:size 72 #:face "Courier" #:weight 'bold #:family 'modern))
(define title-font-a (make-font #:size 72 #:face "Courier" #:weight 'normal #:family 'modern))
(define title-font-b (make-font #:size 40 #:face "Courier" #:weight 'normal #:family 'modern))

; Misc Functions
(define (clear-screen dc width height)
    (send dc set-pen no-pen)
    (send dc set-brush black-brush)
    (send dc draw-rectangle 0 0 width height))

