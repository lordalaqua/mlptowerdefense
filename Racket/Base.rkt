#lang racket

(require "Draw.rkt")
(require "Entity.rkt")
(require "Point.rkt")

(provide (all-defined-out))

; Player Base
; An entity that does not move, but has HP (hit points)
; HP : number
(struct base entity (hp))

; Updating the base is only subtracting the damage done
(define (damage-base this damage)
    (base
        (entity-pos this)
        (entity-text this)
        (entity-angle this)
        (- (base-hp this) damage)))

; Draw the base
(define (draw-base dc base)
    (define base-size 120)
    (send dc set-pen dot-pen)
    (send dc set-brush black-brush)
    ; A black square around base position
    (send dc draw-rectangle
        (- (pt-x (entity-pos base)) (/ base-size 2))
        (- (pt-y (entity-pos base)) (/ base-size 2))
        base-size
        base-size)
    ; Use a larger font for the base entity (usually a lambda character)
    (send dc set-font base-font)
    (draw-entity dc base)
    (send dc set-font main-font)

    ; Draw base's remaining HP
    (send dc draw-text
        (string-append
            "HP: "
            (number->string (base-hp base)))
        (- (pt-x (entity-pos base)) 35)
        (+ (pt-y (entity-pos base)) 30))

    ; Reset brush
    (send dc set-brush no-brush))